<?php
session_start();
if(isset($_GET["abort_request"])):
	ignore_user_abort();
	ob_flush();flush();
endif;

set_time_limit(10);
ini_set('max_execution_time', 10);


if(!empty($_REQUEST["method"])):
	
	switch ($_REQUEST["method"]) {
		case 'main':
			# code...
			include 'class/user.php';
			$user = new User();		
			break;

		case 'dashboard':
			# code...
			include 'class/dashboard_model.php';
			$dashboard = new Dashboard_model();		
			break;

		case 'announcements':
			# code...
			include 'class/announcements_model.php';
			$announcements = new Announcements_model();		
			break;

		case 'events':
			# code...
			include 'class/events_model.php';
			$events = new Events_model();		
			break;
		
		default:
			# code...
			break;
	}
	
	$method = $_REQUEST["method"];
	require_once 'class/global_function.php';
	require_once("controller/".$method.".php");	
	new $method($_REQUEST);
endif;
?>