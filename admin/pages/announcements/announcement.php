<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">                           
                <?php if ($announcements != null) : ?>
                    <?php foreach ($announcements as $announcement) : ?>
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <?=$announcement["title"]?>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        <?php
                                            if( strlen( $announcement["content"] ) > 50 ) { $announcement["content"] = substr( $announcement["content"], 0, 50 ) . '...'; }
                                            echo $announcement["content"]; 
                                        ?>
                                    </p>
                                    <div style="font-size: 12px; ">
                                        <p><em> - <?=$announcement["author"]?> <?php echo cleanDateTime($announcement["date_posted"])?></em></p>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="#" onclick="editPost('announcements','view_edit',<?=$announcement['id']?>);return false;" class="btn btn-info btn-xs" title="Edit Announcement"><i class="fa fa-edit fa-fw"></i></a>
                                    <a href="" onclick="deletePost('announcements','delete',<?=$announcement['id']?>); return false;" class="btn btn-danger btn-xs" title="Delete Announcement"><i class="fa fa-trash-o fa-fw"></i></a>
                                    <a href="#" onclick="viewPost('announcements','view',<?=$announcement['id']?>); return false;" class="btn btn-default btn-xs" title="View Announcement"><i class="fa fa-arrow-circle-o-right fa-fw"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="alert alert-danger">No announcemebts has been posted.</div>
                <?php endif; ?>                    

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
