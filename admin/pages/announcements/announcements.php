
            <div class="row">
                <div class="col-lg-12">
                    <div class="loading"></div>
                    <h1 class="page-header">Announcements</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="col-lg-6 pull-left" style="margin-bottom: 20px;">
                <select name="filter-post" class="form-control" onchange="filterPost('announcements',this.value)">
                    <option value="All">All</option>
                    <?php foreach ($departments as $dept) : ?>
                        <option value="<?=$dept['id']?>"><?=$dept['dept_name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-lg-6 pull-left" style="text-align: right;">
                <a id="btn-add-announcement" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-info btn-sm" style="text-align: right" onclick="checkCollapse(this);"><i class="fa fa-plus fa-fw"></i></a>
            </div>

            <div class="clearfix"></div>

            <div id="collapseOne" class="panel-collapse collapse">
               <div class="panel panel-default">
                    <div class="panel-heading"> Add New Announcement
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">                           
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">

                            <form id="form" method="POST" action="" onsubmit="AjaxObject.startRequest('post','ajax_event.php?abort_request=1&method=announcements&task=addPost','form'); return false;" role="form">
                                <input type="hidden" name="author" value="<?=$_SESSION["user"][0]["type"]?>">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Title" name="title" type="text" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="content" placeholder="Content" requried></textarea>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="dept" onchange="getCourse(this.value);">
                                            <option value="-">Select Department</option>
                                            <option value="All">All</option>
                                            <?php foreach ($departments as $dept) : ?>
                                                <option value="<?=$dept['id']?>"><?=$dept['dept_name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="course" name="course" disabled>
                                            <option value="-">Select Course</option>
                                        </select>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" class="btn btn-sm btn-success btn-block" name="sub_login" value="Add Announcement" />
                                </fieldset>
                            </form>
                            
                        </div>
                        <div class="col-lg-3"></div>                            
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel --> 
            </div> <!--<div id="collapseOne" class="panel-collapse collapse">-->


            <div class="announcement-post"></div>