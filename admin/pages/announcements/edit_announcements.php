<?php foreach ($announcements as $announcement) : ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Edit <?=$announcement["title"]?></h4>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <form id="edit_form" method="POST" action="" onsubmit="AjaxObject.startRequest('post','ajax_event.php?abort_request=1&method=announcements&task=editPost','edit_form'); return false;" role="form">
                                <input type="hidden" name="id" value="<?=$announcement['id']?>">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Title" name="title" type="text" value="<?=$announcement["title"]?>" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="content" placeholder="Content" requried><?=$announcement["content"]?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="dept" onchange="getCourse(this.value);" readonly>
                                            <option value="<?=$announcement["dept"]?>" selected>
                                                <?php
                                                    if ($announcement["dept"] != "All") echo getDeptName($announcement["dept"]);
                                                    else echo $announcement["dept"];
                                                ?>    
                                            </option>
                                            <option value="-">Select Department</option>
                                            <option value="All">All</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="course" name="course" readonly>
                                            <option value="<?=$announcement["course"]?>" selected>
                                                <?php
                                                    if ($announcement["course"] != "All") echo getCourseName($announcement["course"]);
                                                    else echo $announcement["course"];
                                                ?>    
                                            </option>                                            
                                        </select>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" class="btn btn-sm btn-success btn-block" name="submit" value="Edit Announcement" />
                                </fieldset>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php endforeach; ?>