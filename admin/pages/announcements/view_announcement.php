<?php foreach ($announcements as $announcement) : ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?=$announcement["title"]?></h4>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p><?=$announcement["content"]?></p>

                            <div style="font-size: 12px; ">
                                <p><em> - <?=$announcement["author"]?> <?php echo cleanDateTime($announcement["date_posted"])?></em></p>
                            </div>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php endforeach; ?>