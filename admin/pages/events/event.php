<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">                           
                <?php if ($events != null) : ?>
                    <?php foreach ($events as $event) : ?>

                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <?=$event["title"]?>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        <?php
                                            if( strlen( $event["content"] ) > 50 ) { $event["content"] = substr( $event["content"], 0, 50 ) . '...'; }
                                            echo $event["content"]; 
                                        ?>
                                    </p>
                                    <div style="font-size: 12px; ">
                                        <p><em> - <?=$event["author"]?> <?php echo cleanDateTime($event["date_posted"])?></em></p>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="#" onclick="editPost('events','view_edit',<?=$event['id']?>);return false;" class="btn btn-info btn-xs" title="Edit Event"><i class="fa fa-edit fa-fw"></i></a>
                                    <a href="" onclick="deletePost('events','delete',<?=$event['id']?>); return false;" class="btn btn-danger btn-xs" title="Delete Event"><i class="fa fa-trash-o fa-fw"></i></a>
                                    <a href="#" onclick="viewPost('events','view',<?=$event['id']?>); return false;" class="btn btn-default btn-xs" title="View Event"><i class="fa fa-arrow-circle-o-right fa-fw"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="alert alert-danger">No events has been posted.</div>
                <?php endif; ?>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
