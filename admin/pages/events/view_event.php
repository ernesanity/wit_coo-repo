<?php foreach ($events as $event) : ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?=$event["title"]?></h4>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p><?=$event["content"]?></p>

                            <div style="font-size: 12px; ">
                                <p><em> - <?=$event["author"]?> <?php echo cleanDateTime($event["date_posted"])?></em></p>
                            </div>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php endforeach; ?>