$(document).ready(function() {

	//make ajax request as object
	AjaxObject = {
		startRequest: function(t, u, form) {
			postData = $("#"+form).serialize();
			//console.log(u);
			$.ajax({
				type: t,
				url: u,
				data: postData,
				dataType: "JSON",
				beforeSend: function() {
					$('.loading').html('<div style="margin-bottom: -20%;margin-top: 20%;"><img src="assets/img/ajax-loader.gif" style="width: 25px;" /></div>');
				},
				success: function(d) {
					//console.log(d);
					$('.loading').html(d['html']);
					eval(d['javascript']);
				}
			});

			return false;
		}
	} //AjaxObject = {

checkCollapse = function(e) {
	
	id = $(e).attr("id");
	console.log(id);
	
	$("#"+id+" i").attr("class","fa fa-times fa-fw");

	if ($("#collapseOne").hasClass('in')) {
		$("#"+id+" i").attr("class","fa fa-plus fa-fw");
	}

}

announcements = function() {
	//console.log('ajax_event.php?abort_request=1&method=announcements&task=getAll');
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method=announcements&task=getAll',
		dataType: "JSON",
		beforeSend: function () {
			$(".loading").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
		},
		success: function(d) {
			$(".loading").html("");
			$(".announcement-post").html(d["html"]);
		}
	});

}

events = function() {
	//console.log('ajax_event.php?abort_request=1&method=announcements&task=getAll');
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method=events&task=getAll',
		dataType: "JSON",
		beforeSend: function () {
			$(".loading").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
		},
		success: function(d) {
			$(".loading").html("");
			$(".events-post").html(d["html"]);
		}
	});

}

deletePost = function(controller, task, id) {

	//$("#viewPostModal").modal("show");

	if (confirm('Are you sure you want to delete this?')) {
		$.ajax({
			url: 'ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id,
			dataType: 'JSON',
			beforeSend: function() {
				$(".loading").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
			},
			success: function(d) {
				//console.log(d);
				$(".loading").html(d["html"]);
				eval(d["javascript"]);
			}
		});
	} 

	//console.log('ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id);
}

editPost = function(controller, task, id) {

	$("#viewPostModal").modal("show");
	console.log('ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id);
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id,
		dataType: 'JSON',
		beforeSend: function() {
			$("#viewPostModal .modal-body").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
		},
		success: function(d) {
			//console.log(d);
			$("#viewPostModal .modal-body").html(d["html"]);
		}
	});

}

viewPost = function(controller, task, id) {

	$("#viewPostModal").modal("show");
	//console.log('ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id);
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method='+controller+'&task='+task+'&id='+id,
		dataType: 'JSON',
		beforeSend: function() {
			$("#viewPostModal .modal-body").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
		},
		success: function(d) {
			//console.log(d);
			$("#viewPostModal .modal-body").html(d["html"]);
		}
	});

}

getCourse = function(id) {
	option = "";
	if (id == "-") {
		alert("Oops. Please select a valid department!");
	} else {
	//console.log('ajax_event.php?abort_request=1&method=announcements&task=getCourse&id='+id);
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method=announcements&task=getCourse&id='+id,
		dataType: 'JSON',
		success: function(d) {
			console.log(d["html"]);
			option += "<option value='-'>Select Course</option>";
			option += "<option value='All'>All</option>";
			option += d["html"];
			$("#course").html(option).removeAttr("disabled");
		}
	});


	}
}

filterPost = function(controller, id) {
	console.log('ajax_event.php?abort_request=1&method='+controller+'&task=get_announcement&id='+id);
	$.ajax({
		url: 'ajax_event.php?abort_request=1&method=announcements&task=get_announcement&id='+id,
		dataType: "JSON",
		beforeSend: function () {
			$(".loading").html("<div class='alert alert-warning'>Processing data. Please wait.</div>");
		},
		success: function(d) {
			$(".loading").html("");
			$(".announcement-post").html(d["html"]);
		}
	});
}

});