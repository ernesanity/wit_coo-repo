$(document).ready(function(){

	var currentRequest = null;

	sideBar = function(t) {

		if (t) { //if sidebar is allowed to show
			$("#side-menu").show();
			$("#page-wrapper").attr("style","margin: 0 0 0 250px !important;");
		} else {
			$("#side-menu").hide();
			$("#page-wrapper").attr("style","margin: 0 0 0 0 !important;");
		}

	}	

	pageRender = function() {

		hash = window.location.hash;
		if(hash.length <=0) hash = "#main"; //default page		
		hash = hash.replace("#","");
		hash = hash.split("/");
		if(typeof hash[1] == "undefined") hash[1] = ""; // default page
		if(typeof hash[2] == "undefined") hash[2] = "";
		page = hash[0];

		switch(page){
			case 'main': case'login': case 'logout':
				sideBar(false)
				break;
			default :
				sideBar(true)
				break;
		}

		$("#side-menu a").each(function(){
			//console.log(hash+" "+$(this).attr("href"));
			if( hash == $(this).attr("href")){
				$(this).addClass("active");
			}else{
				$(this).removeClass("active");
			}
			//console.log($(this).parent());
		});

		//console.log(currentRequest);
		if(currentRequest != null) {
			currentRequest.abort();
		}

		switch(page) {
			case 'main':
				console.log('ajax_event.php?abort_request=1&method=main&task=');
				currentRequest = $.ajax({
					url: 'ajax_event.php?abort_request=1&method=main&task=',
					dataType: "JSON",
					beforeSend: function() {
						$("#page-wrapper").html('<img src="assets/img/ajax-loader.gif" style="display: block;margin-left: auto;margin-right: auto;width: 50px;padding-top: 50px;" />');
					},
					success: function(d) {
						//setTimeout(function(){
							//console.log(d["html"]);
							$("#page-wrapper").fadeTo("slow","1");
							$("#page-wrapper").html(d["html"]);
							eval(d["javascript"]);
						//},1000);
					}
				});				

				break;

			default :
				console.log('ajax_event.php?abort_request=1&method='+page+'&task='+hash[1]+'&'+hash[2]);
				currentRequest = $.ajax({
					url: 'ajax_event.php?abort_request=1&method='+page+'&task='+hash[1]+'&'+hash[2],
					dataType: "JSON",
					beforeSend: function() {
						$("#page-wrapper").html('<img src="assets/img/ajax-loader.gif" style="display: block;margin-left: auto;margin-right: auto;width: 50px;padding-top: 50px;" />');
					},
					success: function(d) {
						//setTimeout(function(){
							//console.log(d["html"]);
							$("#page-wrapper").fadeTo("slow","1");
							$("#page-wrapper").html(d["html"]);
							eval(d["javascript"]);
						//},1000);
					}
				});	

				break;
		}



	}

	pageRender();

	window.onhashchange = function(){

		pageRender();		

	}	

});