<?php
class Main {
	
	static $data;
	public function __construct($d){
		self::$data = $d;
		
		if(empty($d["task"])) $task = "index";
		else $task = $d["task"];
	
		self::$task();
	}

	public function index() {

		$array["html"] = template('pages/common/default.php', null);
		echo json_encode($array);

	}

	public function login() {
		
		global $user;
		
		$login = $user->login($_POST['username'], md5($_POST['password']));

		if ($login) {
			$array["html"] = "<div class='alert alert-success' style='margin-bottom: -20%;margin-top: 20%;'>Account found! Please wait while we redirect you to dashboard...</div>";
			$array["javascript"] = "setTimeout(function(){ window.location.href='index.php#dashboard' },1500);";
		} else {
			$array["html"] = "<div class='alert alert-danger' style='margin-bottom: -20%;margin-top: 20%;'>Incorrect username or password. Please try again.</div>";
		}
		echo json_encode($array);

	}


}