<?php
class Events {
	
	static $data;
	public function __construct($d){
		self::$data = $d;
		
		if(empty($d["task"])) $task = "index";
		else $task = $d["task"];
			
		self::$task();
	}

	public function index() {

		global $events;		

		$data["departments"] = $events->getDept();
		$array["html"] = template('pages/events/events.php', $data);
		$array["javascript"] = "events();";
		echo json_encode($array);

	}

	public function getAll() {

		global $events;

		$data["events"] = $events->getAll();
		$data["departments"] = $events->getDept();
		//$data["courses"] = $announcements->getCourse();		

		$array["html"] = template('pages/events/event.php', $data);
		//$array["javascript"] = "announcements();";
		echo json_encode($array);

	}

	public function get_announcement() {

		$id = self::$data["id"];

		global $announcements;

		$data["announcements"] = $announcements->getAnnouncement($id);

		$array["html"] = template('pages/announcements/announcement.php', $data);
		echo json_encode($array);	

	}

	public function addPost() {

		global $events;

		$addPost = $events->add($_POST['title'],$_POST['content'],$_POST['dept'],$_POST['course'],$_POST['author']);

		if ($addPost) {
			$array["html"] = "<div class='alert alert-success'>Event has been added.</div>";
			$array["javascript"] = "setTimeout(function(){ events(); $('#btn-add-event').click();},1000);";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to add event. Please try again later.</div>";
		}
		echo json_encode($array);

	}

	public function editPost() {

		global $events;

		$editPost = $events->editPost($_POST['title'],$_POST['content'],$_POST['id']);

		if ($editPost) {
			$array["html"] = "<div class='alert alert-success'>Event has been update.</div>";
			$array["javascript"] = "setTimeout(function(){ events(); },2000); $('#viewPostModal').modal('hide');";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to update event. Please try again later.</div>";
		}
		echo json_encode($array);

	}

	public function view_edit() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $events;

		$data["events"] = $events->getPost($id);

		$array["html"] = template('pages/events/edit_events.php', $data);
		echo json_encode($array);

	}

	public function delete() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		if ($announcements->deletePost($id)) {
			$array["html"] = "<div class='alert alert-success'>Announcement has been deleted.</div>";
			$array["javascript"] = "setTimeout(function(){ pageRender(); },1500);";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to delete announcement. Please try again later.</div>";
		}
		
		echo json_encode($array);

	}

	public function view() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $events;

		$data["events"] = $events->getPost($id);

		$array["html"] = template('pages/events/view_event.php', $data);
		echo json_encode($array);

	}

	public function getCourse() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		$data["courses"] = $announcements->getCourse($id);

		$arr = [];
		foreach ($data["courses"] as $course) :
			$arr[] = "<option value='".$course['id']."'>".$course['course_name']."</option>";
		endforeach;
		
		$array["html"] = ($arr);
		echo json_encode($array);

	}

}