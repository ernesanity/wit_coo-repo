<?php
class Announcements {
	
	static $data;
	public function __construct($d){
		self::$data = $d;
		
		if(empty($d["task"])) $task = "index";
		else $task = $d["task"];
			
		self::$task();
	}

	public function index() {

		global $announcements;		

		$data["departments"] = $announcements->getDept();
		$array["html"] = template('pages/announcements/announcements.php', $data);
		$array["javascript"] = "announcements();";
		echo json_encode($array);

	}

	public function getAll() {

		global $announcements;

		$data["announcements"] = $announcements->getAll();
		$data["departments"] = $announcements->getDept();
		//$data["courses"] = $announcements->getCourse();		

		$array["html"] = template('pages/announcements/announcement.php', $data);
		//$array["javascript"] = "announcements();";
		echo json_encode($array);

	}

	public function get_announcement() {

		$id = self::$data["id"];

		global $announcements;

		$data["announcements"] = $announcements->getAnnouncement($id);

		$array["html"] = template('pages/announcements/announcement.php', $data);
		echo json_encode($array);	

	}

	public function addPost() {

		global $announcements;

		$addPost = $announcements->add($_POST['title'],$_POST['content'],$_POST['dept'],$_POST['course'],$_POST['author']);

		if ($addPost) {
			$array["html"] = "<div class='alert alert-success'>Announcement has been added.</div>";
			$array["javascript"] = "setTimeout(function(){ announcements(); $('#btn-add-event').click();},1000);";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to add announcement. Please try again later.</div>";
		}
		echo json_encode($array);

	}

	public function editPost() {

		global $announcements;

		$editPost = $announcements->editPost($_POST['title'],$_POST['content'],$_POST['id']);

		if ($editPost) {
			$array["html"] = "<div class='alert alert-success'>Announcement has been update.</div>";
			$array["javascript"] = "setTimeout(function(){ announcements(); },2000); $('#viewPostModal').modal('hide');";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to update announcement. Please try again later.</div>";
		}
		echo json_encode($array);

	}

	public function view_edit() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		$data["announcements"] = $announcements->getPost($id);

		$array["html"] = template('pages/announcements/edit_announcements.php', $data);
		echo json_encode($array);

	}

	public function delete() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		if ($announcements->deletePost($id)) {
			$array["html"] = "<div class='alert alert-success'>Announcement has been deleted.</div>";
			$array["javascript"] = "setTimeout(function(){ pageRender(); },1500);";
		} else {
			$array["html"] = "<div class='alert alert-danger'>Oops! Something went wrong while trying to delete announcement. Please try again later.</div>";
		}
		
		echo json_encode($array);

	}

	public function view() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		$data["announcements"] = $announcements->getPost($id);

		$array["html"] = template('pages/announcements/view_announcement.php', $data);
		echo json_encode($array);

	}

	public function getCourse() {
		
		$id = self::$data["id"];
		//$v   = self::$data["v"];

		global $announcements;

		$data["courses"] = $announcements->getCourse($id);

		$arr = [];
		foreach ($data["courses"] as $course) :
			$arr[] = "<option value='".$course['id']."'>".$course['course_name']."</option>";
		endforeach;
		
		$array["html"] = ($arr);
		echo json_encode($array);

	}

}