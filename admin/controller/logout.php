<?php
class Logout {

	static $data;
	public function __construct($d){
		self::$data = $d;
		
		if(empty($d["task"])) $task = "index";
		else $task = $d["task"];
	
		self::$task();
	}

	public function index() {

		$array["html"] = template('pages/common/logout.php', null);
		echo json_encode($array);

	}

	public function foo() {

		echo "function foo";

	}


}