<?php
class Dashboard {
	
	static $data;
	public function __construct($d){
		self::$data = $d;
		
		if(empty($d["task"])) $task = "index";
		else $task = $d["task"];
	
		self::$task();
	}

	public function index() {

		$array["html"] = template('pages/dashboard/dashboard.php', null);
		echo json_encode($array);

	}



}