<?php
include 'config/config.php';

class Events_model {

	public function test() {
		return "foo";
	}

	public function add($title, $content, $dept, $course, $author) {

		global $dbh;

		$datetime = date("Y-m-d h:i:s");

		$stmt = $dbh->prepare("INSERT INTO events (title, content, author, dept, course, date_posted) VALUES(:title, :content, :author, :dept, :course, :date_posted)");
		$stmt->execute(array(":title" => $title, ":content" => $content, ":author" => $author, ":dept" => $dept, ":course" => $course, ":date_posted" => $datetime));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function editPost($title, $content, $id) {

		global $dbh;

		$datetime = date("Y-m-d h:i:s");

		$stmt = $dbh->prepare("UPDATE events SET title=:title, content=:content WHERE id=:id LIMIT 1");
		$stmt->execute(array(":title" => $title, ":content" => $content, ":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public function getAll() {

		global $dbh;

		$stmt = $dbh->prepare("SELECT * FROM events");
		$stmt->execute();

		$result = $stmt->fetchAll();

		return $result;

	}

	public function getAnnouncement($id) {

		global $dbh;

		if ($id == "All") {
			$stmt = $dbh->prepare("SELECT * FROM announcements");
			$stmt->execute();
		} else {
			$stmt = $dbh->prepare("SELECT * FROM announcements WHERE dept=:dept");
			$stmt->execute(array(":dept" => $id));
		}

		$result = $stmt->fetchAll();

		return $result;

	}

	public function getPost($id) {

		global $dbh;

		$stmt = $dbh->prepare("SELECT * FROM events WHERE id=:id LIMIT 1");
		$stmt->execute(array(":id" => $id));

		$result = $stmt->fetchAll();

		return $result;	

	}

	public function deletePost($id) {

		global $dbh;

		$stmt = $dbh->prepare("DELETE FROM announcements WHERE id=:id");
		$stmt->execute(array(":id" => $id));

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}	

	}

	public function getDept() {

		global $dbh;

		$stmt = $dbh->prepare("SELECT * FROM department");
		$stmt->execute();

		$result = $stmt->fetchAll();

		return $result;	

	}

	public function getCourse($id) {

		global $dbh;

		$stmt = $dbh->prepare("SELECT * FROM course WHERE dept_id=:id");
		$stmt->execute(array(":id" => $id));

		$result = $stmt->fetchAll();

		return $result;		

	}

	public function getCourseName($id) {

		global $dbh;

		$stmt = $dbh->prepare("SELECT course_name FROM course WHERE id=:id");
		$stmt->execute(array(":id" => $id));

		$result = $stmt->fetchAll();

		return $result;		

	}

}