<?php
include 'config/config.php';

class Dashboard_model {

	public function test() {
		return "foo";
	}

	public function login($username, $password) {
		
		global $dbh;

		$stmt = $dbh->prepare("SELECT * FROM admin WHERE username=:username AND password=:password");
		$stmt->execute(array(":username" => $username, ":password" => $password));

		if ($stmt->rowCount() > 0) {

			$result = $stmt->fetchAll();
			//$array = [];
			foreach ($result as $key => $value) {
				# code...
				$_SESSION["user"][$key] = $value;
			}

			return true;
		} else {
			return false;
		}

	}

}