<?php
//include 'config/config.php';

function foo() {
	echo "foo function in global_function.php";
}

function template($template_9281812, $array=null){
	
	if(is_file($template_9281812)){
		
		if(is_array($array)){
			extract($array);
		}
		ob_start();
		
		include $template_9281812;
		
		#$template_9281812 = ob_get_clean();
    
	    $template_9281812 = ob_get_contents();
	    ob_end_clean();

    #$template_9281812 = iconv("UTF-8","ISO-8859-1//IGNORE",$template_9281812);
    #$template_9281812 = mb_convert_encoding($template_9281812,"UTF-8","auto");
		return $template_9281812;
	}
}

function getDeptName($id) {

	global $dbh;

	$stmt = $dbh->prepare("SELECT dept_name FROM department WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	$result = $stmt->fetchColumn();

	return $result;

}

function getCourseName($id) {

	global $dbh;

	$stmt = $dbh->prepare("SELECT course_name FROM course WHERE id=:id LIMIT 1");
	$stmt->execute(array(":id" => $id));

	$result = $stmt->fetchColumn();

	return $result;

}

function cleanDateTime($data) {

	$dateTime = new DateTime($data);
	$cleanDate = $dateTime->format('F j, Y h:i:A'); // 15th Apr 2010

	return $cleanDate;	

}

function print_debug($data) {

	echo "<pre>";
	print_r($data);
	echo "</pre>";

}