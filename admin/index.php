<?php
session_start();
include 'config/config.php';
include 'class/global_function.php';
include 'class/session.php';

if (isset($_GET["logout"]) == "true") {
    unset($_SESSION["user"]);
    header("Location:".BASEPATH."#");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WIT</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=BASEPATH?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css?<?php echo mt_rand(); ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=BASEPATH?>assets/bower_components/metisMenu/dist/metisMenu.min.css?<?php echo mt_rand(); ?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?=BASEPATH?>assets/dist/css/timeline.css?<?php echo mt_rand(); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=BASEPATH?>assets/dist/css/sb-admin-2.css?<?php echo mt_rand(); ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?=BASEPATH?>assets/bower_components/morrisjs/morris.css?<?php echo mt_rand(); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=BASEPATH?>assets/bower_components/font-awesome/css/font-awesome.min.css?<?php echo mt_rand(); ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">WIT</a>
            </div>
            <!-- /.navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu" style="display: none;">
                        <li>
                            <a href="#dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#announcements" class="active"><i class="fa fa-microphone fa-fw"></i> Announcements</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#events"><i class="fa fa-table fa-fw"></i> Events</a>
                        </li>
                        <li>
                            <a href="#calendar"><i class="fa fa-calendar-o fa-fw"></i> Calendar</a>
                        </li>
                        <li>
                            <a href="#users"><i class="fa fa-users fa-fw"></i> Accounts</a>
                        </li>
                        <?php if (isset($_SESSION["user"])) : ?>
                        <li>
                            <a href="?logout=true"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php endif; ?>                            
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper" style="margin: 0 0 0 0 !important;">
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include 'pages/templates/modals.php'; ?>

    <!-- jQuery -->
    <script src="<?=BASEPATH?>assets/bower_components/jquery/dist/jquery.min.js?<?php echo mt_rand(); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=BASEPATH?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js?<?php echo mt_rand(); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=BASEPATH?>assets/bower_components/metisMenu/dist/metisMenu.min.js?<?php echo mt_rand(); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <!--script src="<?=BASEPATH?>assets/bower_components/raphael/raphael-min.js"></script>
    <script src="<?=BASEPATH?>assets/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?=BASEPATH?>assets/js/morris-data.js"></script-->

    <!-- Custom Theme JavaScript -->
    <script src="<?=BASEPATH?>assets/dist/js/sb-admin-2.js?<?php echo mt_rand(); ?>"></script>

    <!-- Custom JavaScript -->
    <script src="<?=BASEPATH?>assets/dist/js/script.js?<?php echo mt_rand(); ?>"></script>    
    <script src="<?=BASEPATH?>assets/dist/js/page.js?<?php echo mt_rand(); ?>"></script>    

</body>

</html>
